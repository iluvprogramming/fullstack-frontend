import './App.css';
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Navbar from './layout/Navbar';
import Home from './pages/Home';
import{BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AddUser from './users/AddUser';
import EditUser from './users/EditUser';
import Login from './pages/Login';
import ViewUser from "./users/ViewUser";
import ForgotPassword from './pages/ForgotPassword';

function App() {
  return (
    <div className="App">
      <Router>
        <Navbar/>
        <Routes>
          <Route exact path='/' element={<Login/>}/>
          <Route exact path='/home' element={<Home/>}/>
          <Route exact path='/adduser' element={<AddUser/>}/>
          <Route exact path="/edituser/:id" element={<EditUser />} />
          <Route exact path="/viewuser/:id" element={<ViewUser />} />
          <Route exact path="/forgotpassword/" element={<ForgotPassword />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
